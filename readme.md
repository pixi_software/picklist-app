# pixi* Picklist App 2.0

-----

New Features:

##  "Refill" Picklist / Replenishment
Refill is the "reverse" process to picking, it's getting items from your Replenishment-location down (or over) to your pick location.

At the moment the location IDs are hard coded. So it will always pick FromLoc 001 and try to put stuff ToLoc 002. It uses the existing "Nachfüllbericht" in pixi for that.

## Bin-Colour-Coding

Seen this nice idea at VDS. They use a colour code on the bin sub-names.

![](bin-colour-codig.jpg)

Now the app showes these colours based on the last part of the bin name.

So it's really easy to pick the right bin :-)

![](colour1.png)
![](colour2.png)
![](colour3.png)

The Colour codes are defined in the picklist.php in the Array "$bin_colours" - they check for the last character of the BinName - and work for Numbers and characters.

---

## Ideas

Sometimes its important to also see the customers that belong to a "pick" or order. E.g. we have customers that work with batches, that would need that feature.

As we have this info saved in PLDetail table and AFAIK we also get it through the api call (and have some special code to group them together again) this should be a piece of cake!

![](ShowingCustomers.png)
