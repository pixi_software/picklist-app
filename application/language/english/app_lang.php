<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

$lang['confirm_amount'] = 'Did you take <strong>%1$s</strong> pieces?';
$lang['total_picked'] = '%1$d Item(s) from picklist %2$s picked.';
$lang['total_refill'] = '%1$d Item(s) picked.';
$lang['is_working_on_picklist'] = '%1$s is working on the picklist.';
$lang['app_title'] = 'Picklist App';
$lang['picklist_completed'] = 'Picklist completed';
$lang['bin_shortfall'] = 'Shortfall';
$lang['created_on'] = 'Created on';
$lang['expires'] = 'Expires on';
$lang['created_by'] = 'Created by';
$lang['start_end_bin'] = 'Bin from | to';
$lang['items'] = 'Items';
$lang['comment'] = 'Comment';
$lang['picklist'] = 'Picklist';
$lang['item'] = 'Item';
$lang['bin'] = 'Bin';
$lang['quantity'] = 'Quantity';
$lang['next'] = 'Next';
$lang['of'] = 'of';
$lang['yes'] = 'Yes';
$lang['back'] = 'Back';
$lang['pieces'] = 'pieces';
$lang['in_bin_head'] = 'In bin';
$lang['in_bin'] = 'in bin';
$lang['shortfall_reported'] = 'Shortfall reported';
$lang['logout'] = 'Logout';
$lang['image'] = 'Image';
$lang['solve_shortfall'] = 'Shortfall solved';
$lang['of'] = 'of';
$lang['complete'] = 'complete';
$lang['back_to_startpage'] = 'Back to start page';
$lang['barcode'] = 'Barcode';
$lang['german'] = 'German';
$lang['english'] = 'English';
$lang['close'] = 'Close';
$lang['takeover_picklist'] = 'Takeover picklist';
$lang['picklist_currently_picked'] = 'The picklist is currently picked';
$lang['no_picklist_available'] = 'Currently there is no picklist in your system.';
$lang['save'] = 'Save';
$lang['location'] = 'Location';
$lang['docu_help_link'] = 'https://help.pixi.eu/en/picklist-app';
$lang['docu_help_text'] = 'Open pixi* Help';
$lang['location_from'] = 'From Location';
$lang['location_to'] = 'To Location';
$lang['refill'] = 'Refill items';
$lang['refill_start'] = 'Start picking';
$lang['refill_info'] = 'Picking items to refill';
$lang['refill_complete'] = 'Refill picklist completed';
$lang['more_info'] = 'More info';
$lang['dont_show'] = 'Don\'t show anymore';
$lang['whats_new'] = 'What\'s new?';
$lang['release_note_refill_title'] = 'Refill items';
$lang['release_note_refill_text'] = 'Use the Picklist App for refilling stock between different locations.';
$lang['release_note_colorcodes_title'] = 'Pick by Color';
$lang['release_note_colorcodes_text'] = 'Make it even easier to find the right bin place by color-coding your bins. The Picklist App will highlight bin names with the concerning color.';
$lang['release_notes_docu_link'] = 'http://help.pixi.eu/en/picklist-app';
$lang['color_code_settings'] = 'Configuration of color-coding for bins';
$lang['color_code_settings_link'] = 'https://help.pixi.eu/en/ID18356';
