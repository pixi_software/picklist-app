<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

$lang['confirm_amount'] = 'Wurden <strong>%1$s</strong> Stück entnommen?';
$lang['total_picked'] = '%1$d Artikel von %2$s gepickt.';
$lang['total_refill'] = '%1$d Artikel gepickt.';
$lang['is_working_on_picklist'] = '%1$s arbeitet aktuell an dieser Pickliste.';
$lang['app_title'] = 'Picklisten App';
$lang['picklist_completed'] = 'Pickliste abgearbeitet';
$lang['bin_shortfall'] = 'Fehlbestand';
$lang['created_on'] = 'Erstellt am';
$lang['expires'] = 'Verfällt am';
$lang['created_by'] = 'Erstellt von';
$lang['start_end_bin'] = 'Lagerplatz von | bis';
$lang['items'] = 'Artikel';
$lang['comment'] = 'Kommentar';
$lang['picklist'] = 'Pickliste';
$lang['item'] = 'Artikel';
$lang['bin'] = 'Lagerplatz';
$lang['quantity'] = 'Menge';
$lang['next'] = 'Weiter';
$lang['of'] = 'von';
$lang['yes'] = 'Ja';
$lang['back'] = 'Zurück';
$lang['pieces'] = 'Stück';
$lang['in_bin_head'] = 'Auf dem Lagerplatz';
$lang['in_bin'] = 'auf dem Lagerplatz';
$lang['shortfall_reported'] = 'Fehlbestand gemeldet';
$lang['logout'] = 'Abmelden';
$lang['image'] = 'Bild';
$lang['solve_shortfall'] = 'Fehlbestand korrigiert';
$lang['of'] = 'von';
$lang['complete'] = 'Abschließen';
$lang['back_to_startpage'] = 'Zurück zur Startseite';
$lang['barcode'] = 'Barcode';
$lang['german'] = 'Deutsch';
$lang['english'] = 'Englisch';
$lang['close'] = 'Schließen';
$lang['takeover_picklist'] = 'Pickliste übernehmen';
$lang['picklist_currently_picked'] = 'Die Pickliste wird im Moment gepickt';
$lang['no_picklist_available'] = 'Derzeit gibt es keine Pickliste in Ihrem System';
$lang['save'] = 'Speichern';
$lang['location'] = 'Location';
$lang['docu_help_link'] = 'http://help.pixi.eu/picklisten-app';
$lang['docu_help_text'] = 'pixi* Hilfe öffnen';
$lang['location_from'] = 'Von Location';
$lang['location_to'] = 'Nach Location';
$lang['refill'] = 'Artikel nachfüllen';
$lang['refill_start'] = 'Pickvorgang starten';
$lang['refill_info'] = 'Picken der Artikel zum Nachfüllen';
$lang['refill_complete'] = 'Nachfüllpickliste abgeschlossen';
$lang['more_info'] = 'Weitere Infos';
$lang['dont_show'] = 'Nicht mehr anzeigen';
$lang['whats_new'] = 'Was gibt\'s Neues?';
$lang['release_note_refill_title'] = 'Artikel nachfüllen';
$lang['release_note_refill_text'] = 'Sie können die Picklisten App nun auch zum Nachfüllen des Lagerbestands zwischen verschiedenen Locations verwenden.';
$lang['release_note_colorcodes_title'] = 'Picken nach Farben';
$lang['release_note_colorcodes_text'] = 'Machen Sie es sich leichter, den richtigen Lagerplatz zu finden, indem Sie eine Farbkodierung für Ihre Lagerplätze einführen. Die Picklisten App markiert die Lagerplatznamen mit der zugehörigen Farbe.';
$lang['release_notes_docu_link'] = 'http://help.pixi.eu/picklisten-app';
$lang['color_code_settings'] = 'Konfiguration der Farbkodierung für Lagerplätze';
$lang['color_code_settings_link'] = 'https://help.pixi.eu/ID18357';
