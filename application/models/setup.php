<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Setup extends CI_Model
{
    public function setupTables()
    {
        $this->customerdb->query('
            CREATE TABLE IF NOT EXISTS `stockout` (
                `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `picklist_header_ref` int(11) NOT NULL,
                `ean` varchar(64) NOT NULL,
                `bin_name` varchar(128) NOT NULL,
                `image_link` varchar(256) NOT NULL,
                `total_items_on_bin` int(11) NOT NULL,
                `qty` int(11) NOT NULL,
                `item_name` varchar(250) NOT NULL,
                `item_number_supplier` varchar(120) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        
        $this->customerdb->query('
            CREATE TABLE IF NOT EXISTS `picklist_status` (
                `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `picklist_header_ref` int(11) NOT NULL,
                `username` varchar(64) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        
        $this->customerdb->query('
            CREATE TABLE IF NOT EXISTS `user_settings` (
              `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
              `username` varchar(80) NOT NULL,
              `setting_name` varchar(32) NOT NULL,
              `setting_value` text NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `setting` (`username`,`setting_name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->customerdb->query('
            CREATE TABLE IF NOT EXISTS `colours` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `rgb` varchar(16) NOT NULL,
                `cmyk` varchar(25) NOT NULL,
                `hex` varchar(7) NOT NULL,
                `assigned_character` varchar(10),
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $count_colours = $this->customerdb->count_all('colours');

        if ($count_colours == 0) {
            $colours = array(
                array(
                    'rgb'  => 'rgb(120,28,129)',
                    'cmyk' => 'cmyk(65,100,12,2)',
                    'hex'  => '#781c81',
                ),
                array(
                    'rgb'  => 'rgb(63,78,161)',
                    'cmyk' => 'cmyk(87,80,0,0)',
                    'hex'  => '#3f4ea1',
                ),
                array(
                    'rgb'  => 'rgb(70,131,193)',
                    'cmyk' => 'cmyk(74,42,1,0)',
                    'hex'  => '#4683c1',
                ),
                array(
                    'rgb'  => 'rgb(87,163,173)',
                    'cmyk' => 'cmyk(67,20,31,0)',
                    'hex'  => '#57a3ad',
                ),
                array(
                    'rgb'  => 'rgb(109,179,136)',
                    'cmyk' => 'cmyk(60,10,59,0)',
                    'hex'  => '#6db388',
                ),
                array(
                    'rgb'  => 'rgb(177,190,78)',
                    'cmyk' => 'cmyk(35,12,87,0)',
                    'hex'  => '#b1be4e',
                ),
                array(
                    'rgb'  => 'rgb(223,165,58)',
                    'cmyk' => 'cmyk(13,36,91,0)',
                    'hex'  => '#dfa53a',
                ),
                array(
                    'rgb'  => 'rgb(231,116,47)',
                    'cmyk' => 'cmyk(5,67,93,0)',
                    'hex'  => '#e7742f',
                ),
                array(
                    'rgb'  => 'rgb(217,33,32)',
                    'cmyk' => 'cmyk(9,98,100,1)',
                    'hex'  => '#d92120',
                ),
                array(
                    'rgb'  => 'rgb(119,119,119)',
                    'cmyk' => 'cmyk(55,46,46,11)',
                    'hex'  => '#777777',
                ),
            );
            $this->customerdb->insert_batch('colours', $colours);
        }
        
        
        $fields = $this->customerdb->field_data("stockout");
        
        foreach($fields as $field)
        {
            if($field->name == 'ean' && $field->type == 'int') {
                $this->customerdb->query('ALTER TABLE `stockout` CHANGE `ean` `ean` VARCHAR(64) NOT NULL;');
            }
        }
    }   
}
