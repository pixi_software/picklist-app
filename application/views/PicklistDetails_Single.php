<?php
	$count = 1;
	$total = count($picklistDetails);

	function colour_bin($bin, $bin_colours) {
		$e = explode('-', $bin);
		return isset($bin_colours[end($e)]) ? $bin_colours[end($e)] : 'none';
	}

	foreach($picklistDetails as $picklist) {
	?>

	<div data-role="page" id="article_<?= $count; ?>">

		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
			<div class="navbar-container" id="navbar-container">
				<div class="pull-left">
					<a href="<?= site_url(''); ?>" class="navbar-brand">
						<img class="pixilogo pull-left" src="<?= base_url() ?>/assets/picklist/img/pixiHeader.svg" alt="pixi*" />
						<small class="moduleName"><?= t('app_title'); ?></small>
					</a>
				</div>
				<div class="pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="transparent">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<span><?= $username; ?></span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu-right dropdown-menu">
								<li><a href="/<?php echo session_name(); ?>/?logout" data-ajax="false"><?= t('logout'); ?></a></li>
							</ul>
						</li>
						<li class="transparent helpLink">
							<a href="<?= t('docu_help_link'); ?>" target="_blank" title="<?= t('docu_help_text'); ?>" tabindex="-1">
								<i class="fa fa-question"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="navbar toolbar">
			<div class="navbar-buttons pull-left">
				<ul class="nav">
					<li>
						<a href="<?= site_url(); ?>" data-ajax="false" class="toolbarButton"><i class="fa fa-arrow-circle-o-left"></i></a>
					</li>
					<li>
						<a id="submit_Problem_<?= $count; ?>" data-ajax="true" class="toolbarButton"><i class="fa fa-warning"></i></a>
					</li>
					<li class="">
    					<a href="<?= site_url('settings'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-gear"></i></a>
    				</li>
				</ul>
			</div>
		</div>




		<div class="container-fluid picklistDetail">

				<div class="row">

    				<div class="col-xs-12 col-sm-6">

    					<div class="row">
            				<div class="col-xs-12">
            					<p><?= t('picklist'); ?>: <strong><?= $picklistHeaderKey ?></strong>  |  <?= t('item'); ?> <?= $count.'  '.t('of').' '.$total; ?> <span class="isReported" id="notification_problem_<?= $count; ?>"></span></p>
            				</div>
            			</div>
            			<div class="row" data-role="content" id="content_<?= $count; ?>">
            				<div class="col-xs-12">
            					<?php if($picklist['Qty'] == 1) { ?>
            						<a href="#article_<?= $count + 1 ?>">
    							<?php } else { ?>
        							<a href="#popup_<?= $count; ?>" data-rel="dialog">
    							<?php } ?>
                                        <?php
                                        $colour_bin = colour_bin($picklist['BinName'], $bin_colours);
                                        $bin_text_colour = $colour_bin == 'none' ? 'black' : 'white';
                                        $bin_header_colour = $colour_bin == 'none' ? '#f79c0f' : 'white';
                                        ?>
										<div class="row" style="background-color: <?php echo $colour_bin;?>;">
            								<div class="col-xs-12" >
            									<h5 style="color: <?php echo $bin_header_colour; ?>;"><?= t('bin'); ?></h5>
												<h1 style="color: <?php echo $bin_text_colour; ?>;"><strong><?= $picklist['BinName']; ?></strong></h1>
            								</div>
            							</div>
            							<div class="row">
            								<div class="col-xs-12 col-sm-5 col-md-8 col-lg-8" >
            									<h5><?= t('barcode'); ?></h5>
												<h1><strong style="text-decoration: none;"><?= $picklist['EanUpc']; ?></strong></h1>
            									<h4 class="fontLightGrey"><?= $picklist['ItemNrSuppl']; ?></h4>
            									<p class="fontLightGrey"><?= $picklist['ItemName']; ?></p>
            								</div>
            								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">

            								</div>
            							</div>
            							<div class="row">
            								<div class="col-xs-12">
            									<h5><?= t('quantity'); ?></h5>
            									<h2><strong><?= $picklist['Qty']; ?> <?= t('pieces'); ?> </strong><span class="fontLight fontLightGrey">| <?= $picklist['TotalSimilarItemsOnBin'] ?> <?= t('in_bin'); ?></span></h2>
            								</div>
            							</div>
                					<button class="btn btn-primary btn-lg btnNext" type="submit"> <strong>
                        				<?= ($count != $total) ? '<span class="fontLight">'.t('next').' </span>' . $picklist['nextBin'] : t('complete'); ?>
                        			</strong></button>
                    			</a>
        						<form id="setArticle_Problem_<?= $count; ?>">
        							<input type="hidden" value="<?= $picklist['PLIheaderRef'] ?>" name="PLIheaderRef" />
        							<input type="hidden" value="<?= $picklist['EanUpc'] ?>" name="EanUpc" />
        							<input type="hidden" value="<?= $picklist['BinName'] ?>" name="BinName" />
        							<input type="hidden" value="<?= $picklist['PicLinkLarge'] ?>" name="PicLinkLarge" />
        							<input type="hidden" value="<?= $picklist['Qty'] ?>" name="Qty" />
        							<input type="hidden" value="<?= $picklist['TotalSimilarItemsOnBin'] ?>" name="TotalSimilarItemsOnBin" />
        							<input type="hidden" value="<?= $picklist['ItemName'] ?>" name="ItemName" />
        							<input type="hidden" value="<?= $picklist['ItemNrSuppl'] ?>" name="ItemNrSuppl" />
        						</form>
        					</div>
        				</div>

    				</div>

    				<div style="text-align: right;" class="col-xs-12 col-sm-6">
    					<?php if(isset($picklist['PicLinkLarge']) AND !empty($picklist['PicLinkLarge'])) { ?>
        					<img class="imageContainer" src="<?= $picklist['PicLinkLarge'] ?>" height="80%"
    							onerror="this.onerror=null;this.src='<?= base_url() ?>/assets/picklist/img/no-image.png';" />
    					<?php } else { ?>
    						<img class="imageContainer" src="<?= base_url() ?>/assets/picklist/img/no-image.png" height="80%" />
    					<?php } ?>
    				</div>
    		</div>

    		<script type="text/javascript">
    			<?php if($count > 0) { ?>
    				$(document).on("swiperight", "#article_<?= $count; ?>", function() {
    					$.mobile.changePage( "#article_<?= $count -1; ?>", { transition: "slide", reverse: true });
    				});
    			<?php } ?>

    			$(document).on("swipeleft", "#article_<?= $count; ?>", function() {
    				<?php if($picklist['Qty'] == 1) { ?>
    					$.mobile.changePage( "#article_<?= $count +1; ?>", { transition: "slide", reverse: false });
    					<?php } else { ?>
    					$.mobile.changePage( "#popup_<?= $count; ?>", { transition: "flip", reverse: false });
    				<?php } ?>
    			});

    			$(document).on("click", "#submit_Problem_<?= $count; ?>", function() {

    				var formData = $("#setArticle_Problem_<?php echo $count ?>").serialize();

    				$.ajax({
    					type: "POST",
    					url: "<?= site_url('stockout/reportStockout'); ?>",
    					cache: false,
    					data: formData,
    					success: onSuccess_Problem_<?= $count; ?>
    				});

    				event.stopPropagation();
    			});

    			function onSuccess_Problem_<?= $count; ?>(data, status)
    			{
    				if(data)
    				{
    					$("#notification_problem_<?= $count; ?>").html('<span class="alert alert-success"><i class="fa fa-check"></i> <?= t('shortfall_reported') ?></span>');
    					$("#notification_problem_<?= $count; ?>").fadeIn();
    				}
    				else
    				{
    					$("#notification_problem_<?= $count; ?>").css("background-color", "#ff0000");
    					$("#notification_problem_<?= $count; ?>").text(data);
    				}
    			}

    		</script>
		</div>
	</div>



	<div data-role="dialog" id="popup_<?= $count; ?>">

    <div data-role="content">
	<div class="margin40"></div>
	<div class="margin40"></div>
	<p><?= t('picklist'); ?>: <?= $picklist['PLIheaderRef']; ?> | <?= t('item'); ?> <?php echo $count ?> <?= t('of'); ?> <?= $total; ?></p>
	<h2><?= t('confirm_amount', array($picklist['Qty'])); ?></h2>
	<div class="margin40"></div>
	<a href="#article_<?php echo $count + 1 ?>"  data-role="button" class="btn btn-primary btn-block btn-lg"><h2><?= t('yes'); ?></h2></a>
	<div class="margin20"></div>
	<a onclick="javascript:DepickArticle_<?php echo $count ?>();" data-role="button" class="btn btn-default btn-block" href="#article_<?php echo $count ?>" data-direction="reverse">
	<?= t('back'); ?>
	</a>
    </div>
	</div>

	<?php
    $count++;
	} // End Foreach ?>

	<div data-role="page" class="jqm-demos ui-responsive-panel" id="article_<?= $count; ?>" data-title="Panel responsive page" data-url="panel-responsive-page1">

    <div data-role="content" id="content_<?= $count ?>" class="text-center">
	<div class="margin40"></div>
	<h3 class="text-uppercase"><?= t('picklist_completed'); ?></h3>
	<h2 class="green"><?= t('total_picked', array($total, $picklistHeaderKey)); ?></h2>
	<div id="bcTarget"></div>
	<script>
	$(document).ready(function() {
    	$("#bcTarget").barcode(
    	"PIC<?= $picklistHeaderKey; ?>",
    	"code128",
    	{barWidth:2, barHeight:100}
    	);
	});

	</script>
	<div class="row">
	<div class="col-md-offset-5 col-md-2">
	<a href="<?= site_url(); ?>" data-ajax="false" data-theme="e" class="btn btn-primary btn-lg"><h3><?= t('back_to_startpage'); ?></h3></a>
	</div>
	</div>

    </div>

	</div>

	<script type="text/javascript" src="<?= base_url() ?>/assets/picklist/lib.d/jquery-barcode.min.js"></script>
