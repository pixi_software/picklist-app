<html>
<head>
    <title><?= t('app_title'); ?></title>

	<link type="text/css" rel="stylesheet" href="<?= base_url() ?>/assets/picklist/lib.d/jquery.mobile.structure-1.3.0.min.css" />
    <link rel="stylesheet" href="<?= base_url() ?>/assets/pixi/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url() ?>/assets/pixi/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url() ?>/assets/pixi/css/ace-fonts.css" />
    <link rel="stylesheet" href="<?= base_url() ?>/assets/pixi/css/ace.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/app/css/languages.min.css"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/app/css/main.css"/>

    <link type="text/css" href="<?= base_url() ?>/assets/picklist/lib.d/pixi_picklist_app.css?" media="screen" type="text/css" rel="stylesheet">

<!--    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>/assets/picklist/lib.d/theme_pixi.css" /> -->
   <!-- <link type="text/css" href="<?= base_url() ?>/assets/picklist/lib.d/default.css?" media="screen" type="text/css" rel="stylesheet"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="apple-touch-icon" href="apple-touch-icon.png" />
		<meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes" />
   		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <script type="text/javascript" src="<?= base_url() ?>/assets/picklist/lib.d/jquery-1.12.2.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>/assets/picklist/lib.d/jquery.mobile-1.3.0.js"></script>

</head>
<body>

<?= $body; ?>


<script src="<?= base_url() ?>/assets/picklist/lib.d/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url() ?>/assets/picklist/lib.d/js/ace.min.js"></script>
<script src="<?= base_url() ?>/assets/picklist/lib.d/js/ace-elements.min.js"></script>
<script src="<?= base_url() ?>/assets/picklist/lib.d/js/ace-extra.min.js"></script>

<script language="javascript" type="text/javascript">

    $(document).bind("mobileinit", function () {
        $.mobile.ajaxEnabled = false;
    });

	function showPageLoader() {
        $.mobile.showPageLoadingMsg();
    };
    $(document).ready(function() {
        $(".ui-dialog a[data-icon='delete']").remove();
    });
</script>

<?= Pixi\AppsFactory\GoogleAnalytics::getCode() ?>

</body>
</html>
