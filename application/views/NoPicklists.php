<div data-role="page" id="overview">
	<div id="navbar" class="navbar navbar-default">
		<script type="text/javascript">
			try{ace.settings.check('navbar' , 'fixed')}catch(e){}
		</script>
        <?php echo $releaseNote['script']; ?>
		<div class="navbar-container" id="navbar-container">
			<div class="pull-left">
				<a href="<?= site_url(''); ?>" class="navbar-brand">
					<img class="pixilogo pull-left" src="<?= base_url() ?>/assets/picklist/img/pixiHeader.svg" alt="pixi*" />
					<span class="moduleName"><?= t('app_title'); ?></span>
				</a>
			</div>
			<div class="pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="transparent">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<span><?= $username; ?></span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu-right dropdown-menu">
							<li><a href="<?=current_url()?>?pixi_lang=de" data-ajax="false"><span class="lang-xs" lang="de"></span> &nbsp;<?= t('german'); ?></a></li>
							<li><a href="<?=current_url()?>?pixi_lang=en" data-ajax="false"><span class="lang-xs" lang="en"></span> &nbsp;<?= t('english'); ?></a></li>
							<li class="divider"></li>
							<li><a href="/<?php echo session_name(); ?>/?logout" data-ajax="false"><?= t('logout'); ?></a></li>
						</ul>
					</li>
					<li class="transparent helpLink">
						<a href="<?= t('docu_help_link'); ?>" target="_blank" title="<?= t('docu_help_text'); ?>" tabindex="-1">
							<i class="fa fa-question"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="navbar toolbar">
		<div class="navbar-buttons pull-left">
			<ul class="nav">
				<li class="">
					<button href="<?= site_url('picklist'); ?>" class="toolbarButton" onClick="location.reload();"><i class="fa fa-refresh"></i></button>
				</li>
				<li class="">
					<a href="<?= site_url('settings'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-gear"></i></a>
				</li>
				<li class="">
					<a href="<?= site_url('refill'); ?>" title="<?php echo t('refill'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-exchange"></i></a>
				</li>
			</ul>
		</div>
		<div class="navbar-buttons pull-right">
			<ul class="nav">
				<li class="">
					<a href="<?= site_url('stockout'); ?>" class="toolbarButton" data-axaj="false"><?= t('bin_shortfall'); ?></a>
				</li>
			</ul>
		</div>
	</div>
	<div data-role="content">
	<div class="margin20"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
                <?php echo $releaseNote['body']; ?>
                <h2 class="text-center"><i class="fa fa-info-circle headingIcon"></i><?= t('no_picklist_available'); ?></h2>
            </div>
		</div>
	</div>    
</div>
