<?php
$count = 1;
$total = count($picklistDetails);

foreach ($picklistDetails as $picklist) {
	?>

	<div data-role="page" id="article_<?php echo $count; ?>">

		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try {
					ace.settings.check('navbar', 'fixed')
				} catch (e) {
				}
			</script>
			<div class="navbar-container" id="navbar-container">
				<div class="pull-left">
					<a href="<?php echo site_url(''); ?>" class="navbar-brand">
						<img class="pixilogo pull-left"
							 src="<?php echo base_url() ?>/assets/picklist/img/pixiHeader.svg"
							 alt="pixi*"/>
						<small class="moduleName"><?php echo t('app_title'); ?></small>
					</a>
				</div>
				<div class="pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="transparent">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<span><?php echo $username; ?></span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu-right dropdown-menu">
								<li>
									<a href="<?php echo current_url() ?>?pixi_lang=de" data-ajax="false">
										<span class="lang-xs" lang="de"></span> &nbsp;<?php echo t('german'); ?>
									</a>
								</li>
								<li>
									<a href="<?php echo current_url() ?>?pixi_lang=en" data-ajax="false">
										<span class="lang-xs" lang="en"></span> &nbsp;<?php echo t('english'); ?>
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="/<?php echo session_name(); ?>/?logout" data-ajax="false">
										<?php echo t('logout'); ?>
									</a>
								</li>
							</ul>
						</li>
						<li class="transparent helpLink">
							<a href="<?php echo t('docu_help_link'); ?>" target="_blank"
							   title="<?php echo t('docu_help_text'); ?>"
							   tabindex="-1">
								<i class="fa fa-question"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="navbar toolbar">
			<div class="navbar-buttons pull-left">
				<ul class="nav">
					<li>
						<a href="<?php echo site_url(); ?>" data-ajax="false" class="toolbarButton">
							<i class="fa fa-arrow-circle-o-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>


		<div class="container-fluid picklistDetail">

			<div class="row">

				<div class="col-xs-12 col-sm-6">

					<div class="row">
						<div class="col-xs-12">
							<p><b><?php echo t('refill_info'); ?></b> | <?php echo t('item'); ?> <?php echo $count . '  ' . t('of') . ' ' . $total; ?></p>
						</div>
					</div>
					<div class="row" data-role="content" id="content_<?php echo $count; ?>">
						<div class="col-xs-12">
							<?php if ($picklist['FromBinqty'] == 1) { ?>
							<a href="#article_<?php echo $count + 1 ?>">
								<?php } else { ?>
								<a href="#popup_<?php echo $count; ?>" data-rel="dialog">
									<?php } ?>
									<div class="row">
										<div class="col-xs-12">
											<h5><?php echo t('bin'); ?></h5>
											<h1><strong><?php echo $picklist['FromLocIDbinName']; ?></strong></h1>
										</div>
									</div>
                                    <div class="row">
										<div class="col-xs-12 col-sm-5 col-md-8 col-lg-8">
											<h5><?php echo t('barcode'); ?></h5>
											<h1><strong
													style="text-decoration: none;"><?php echo $picklist['EanUpc']; ?></strong>
											</h1>
											<p class="fontLightGrey"><?php echo $picklist['ItemName']; ?></p>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">

										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<h5><?php echo t('quantity'); ?></h5>
											<h2>
												<strong><?php echo $picklist['MissingQty']; ?> <?php echo t('pieces'); ?> </strong><span
													class="fontLight fontLightGrey">| <?php echo $picklist['FromBinqty']; ?> <?php echo t('in_bin'); ?></span>
											</h2>
										</div>
									</div>
									<button class="btn btn-primary btn-lg btnNext" type="submit"><strong>
											<?php echo ($count != $total) ? '<span class="fontLight">' . t('next') . ' </span>' . $picklist['nextBin'] : t('complete'); ?>
										</strong></button>
								</a>
								<form id="setArticle_Problem_<?php echo $count; ?>">
									<input type="hidden" value="" name="PLIheaderRef"/>
									<input type="hidden" value="<?php echo $picklist['EanUpc'] ?>" name="EanUpc"/>
									<input type="hidden" value="<?php echo $picklist['FromLocIDbinName'] ?>"
										   name="BinName"/>
									<input type="hidden" value="<?php echo $picklist['PicLinkLarge'] ?>"
										   name="PicLinkLarge"/>
									<input type="hidden" value="<?php echo $picklist['FromBinqty'] ?>" name="Qty"/>

									<input type="hidden" value="<?php echo $picklist['ItemName'] ?>" name="ItemName"/>
								</form>
						</div>
					</div>

				</div>

				<div class="col-xs-12 col-sm-6">
					<img class="imageContainer" src="<?php echo $picklist['PicLinkLarge'] ?>" height="80%"
						 onerror="this.onerror=null;this.src='<?php echo base_url() ?>/assets/picklist/img/no-image.png';"/>
				</div>
			</div>

			<script type="text/javascript">
				$(document).on("swiperight", "#article_<?php echo $count; ?>", function () {
					$.mobile.changePage("#article_<?php echo $count - 1; ?>", {transition: "slide", reverse: true});
				});

				$(document).on("swipeleft", "#article_<?php echo $count; ?>", function () {
					$.mobile.changePage("#popup_<?php echo $count; ?>", {transition: "flip", reverse: false});
				});
			</script>
		</div>
	</div>

	<?php
	if ($count == $total) {
		$nextLink = '#overview';
	} else {
		$countNext = $count + 1;
		$nextLink = '#article_' . $countNext;
	}
	?>


	<div data-role="dialog" id="popup_<?php echo $count; ?>">

		<div data-role="content">
			<div class="margin40"></div>
			<div class="margin40"></div>
			<p><?php echo t('refill_info'); ?>:
				| <?php echo t('item'); ?> <?php echo $count ?> <?php echo t('of'); ?> <?php echo $total; ?></p>
			<h2><?php echo t('confirm_amount', array($picklist['MissingQty'])); ?></h2>
			<div class="margin40"></div>
			<a href="<?php echo $nextLink; ?>" data-role="button" class="btn btn-primary btn-block btn-lg">
				<h2><?php echo t('yes'); ?></h2></a>
			<div class="margin20"></div>
			<a data-role="button" class="btn btn-default btn-block" href="#article_<?php echo $count ?>" data-direction="reverse">
				<?php echo t('back'); ?>
			</a>
		</div>
	</div>

	<?php
	$count++;
} // End Foreach ?>

<div data-role="page" id="overview">
	<div id="navbar" class="navbar navbar-default">
		<script type="text/javascript">
			try {
				ace.settings.check('navbar', 'fixed')
			} catch (e) {
			}
		</script>
		<div class="navbar-container" id="navbar-container">
			<div class="pull-left">
				<a href="<?php echo site_url(''); ?>" class="navbar-brand">
					<img class="pixilogo pull-left" src="<?php echo base_url() ?>/assets/picklist/img/pixiHeader.svg"
						 alt="pixi*"/>
					<span class="moduleName"><?php echo t('app_title'); ?></span>
				</a>
			</div>
			<div class="pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="transparent">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<span><?php echo $username; ?></span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu-right dropdown-menu">
							<li>
								<a href="<?php echo current_url() ?>?pixi_lang=de" data-ajax="false">
									<span class="lang-xs" lang="de"></span> &nbsp;<?php echo t('german'); ?>
								</a>
							</li>
							<li>
								<a href="<?php echo current_url() ?>?pixi_lang=en" data-ajax="false">
									<span class="lang-xs" lang="en"></span> &nbsp;<?php echo t('english'); ?>
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/<?php echo session_name(); ?>/?logout" data-ajax="false">
									<?php echo t('logout'); ?>
								</a>
							</li>
						</ul>
					</li>
					<li class="transparent helpLink">
						<a href="<?php echo t('docu_help_link'); ?>" target="_blank"
						   title="<?php echo t('docu_help_text'); ?>"
						   tabindex="-1">
							<i class="fa fa-question"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="navbar toolbar">
		<div class="navbar-buttons pull-left">
			<ul class="nav">
				<li class="">
					<a href="<?php echo site_url('picklist'); ?>" class="toolbarButton" data-ajax="false"><i
							class="fa fa-arrow-circle-o-left"></i></a>
				</li>
			</ul>
		</div>
	</div>
	<div data-role="content">
		<div class="margin20"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">

					<?php if (!$refillChecked) : ?>
						<form action="<?php echo site_url('refill'); ?>" method="POST" data-ajax="false"
							  class="form-horizontal">

							<div class="form-group">
								<label for="location_from"
									   class="col-sm-1 control-label"><?php echo t('location_from'); ?></label>
								<div class="col-sm-11">
									<select name="FromLocID" id="location_from" class="form-control" data-role="none">
										<?php foreach ($locations as $location) { ?>
											<option
												value="<?php echo $location['LocID']; ?>"><?php echo $location['LocID'] . ' - ' . $location['LocName']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="location_to"
									   class="col-sm-1 control-label"><?php echo t('location_to'); ?></label>
								<div class="col-sm-11">
									<select name="ToLocID" id="location_to" class="form-control" data-role="none">
										<?php foreach ($locations as $location) { ?>
											<option
												value="<?php echo $location['LocID']; ?>"><?php echo $location['LocID'] . ' - ' . $location['LocName']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="space"></div>

							<div class="form-group col-xs-12">
								<input type="submit" class="btn btn-primary btn-lg" value="<?php echo t('refill_start'); ?>"/>
							</div>

						</form>

					<?php else : ?>

					<h3 class="text-uppercase"><?php echo t('refill_complete'); ?></h3>
					<h2 class="green"><?php echo t('total_refill', array($total)); ?></h2>
					<div class="row">
						<div class="col-md-offset-5 col-md-2">
							<a href="<?php echo site_url(); ?>" data-ajax="false" data-theme="e"
							   class="btn btn-primary btn-lg"><h3><?php echo t('back_to_startpage'); ?></h3></a>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
