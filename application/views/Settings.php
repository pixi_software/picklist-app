<div data-role="page" id="overview">
    <div id="navbar" class="navbar navbar-default">
        <script type="text/javascript">
            try {
                ace.settings.check('navbar', 'fixed')
            } catch (e) {
            }
        </script>
        <div class="navbar-container" id="navbar-container">
            <div class="pull-left">
                <a href="<?= site_url(''); ?>" class="navbar-brand">
                    <img class="pixilogo pull-left" src="<?= base_url() ?>/assets/picklist/img/pixiHeader.svg"
                         alt="pixi*"/>
                    <span class="moduleName"><?= t('app_title'); ?></span>
                </a>
            </div>
            <div class="pull-right" role="navigation">
                <ul class="nav ace-nav">
                    <li class="transparent">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <span><?= $username; ?></span>
                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu-right dropdown-menu">
                            <li><a href="<?= current_url() ?>?pixi_lang=de" data-ajax="false"><span class="lang-xs"
                                                                                                    lang="de"></span>
                                    &nbsp;<?= t('german'); ?></a></li>
                            <li><a href="<?= current_url() ?>?pixi_lang=en" data-ajax="false"><span class="lang-xs"
                                                                                                    lang="en"></span>
                                    &nbsp;<?= t('english'); ?></a></li>
                            <li class="divider"></li>
                            <li><a href="/<?php echo session_name(); ?>/?logout"
                                   data-ajax="false"><?= t('logout'); ?></a></li>
                        </ul>
                    </li>
                    <li class="transparent helpLink">
                        <a href="<?= t('docu_help_link'); ?>" target="_blank" title="<?= t('docu_help_text'); ?>"
                           tabindex="-1">
                            <i class="fa fa-question"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="navbar toolbar">
        <div class="navbar-buttons pull-left">
            <ul class="nav">
                <li class="">
                    <a href="<?= site_url('picklist'); ?>" class="toolbarButton" data-ajax="false"><i
                            class="fa fa-arrow-circle-o-left"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-buttons pull-right">
            <ul class="nav">
                <li class="">
                    <a href="<?= site_url('stockout'); ?>" class="toolbarButton"
                       data-axaj="false"><?= t('bin_shortfall'); ?></a>
                </li>
            </ul>
        </div>
    </div>
    <div data-role="content">
        <div class="margin20"></div>
        <div class="container-fluid">
            <div class="row">
                <div id="settings" class="col-xs-12 col-sm-6 col-lg-4 pull-left">
                    <form action="<?= site_url('settings'); ?>" method="POST" data-ajax="false" class="form-horizontal">
                        <div class="row">
                            <h2 class="control-label pull-left"><?= t('location'); ?>:</h2>
                        </div>

                        <div class="row">
                            <select name="location" id="location" class="form-control" data-role="none">
                                <?php foreach ($locations as $location) { ?>
                                    <option
                                        value="<?= $location['LocID']; ?>" <?= $location['selected']; ?>><?= $location['LocID'] . ' - ' . $location['LocName']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="space"></div>

                        <div class="row">
                            <h2><?php echo t('color_code_settings'); ?>: <a class="helpLinkIcon" href="#" title="tip:<?php echo t('color_code_settings_link'); ?>">i</a></h2>
                        </div>

                        <div id="colourSettings">
                            <?php foreach ($colours as $colour) { ?>
                                <div class="row item">
                                    <div class="col-xs-3">
                                        <input type="text" name="colours[<?php echo $colour['id']; ?>]"
                                               value="<?php echo $colour['assigned_character']; ?>"
                                               id="colour_<?php echo $colour['id']; ?>" />
                                    </div>
                                    <div class="col-bkg col-xs-9"
                                         style="background-color: <?php echo $colour['hex']; ?>">
                                        <span>
                                        <?php echo $colour['rgb'] . ', ' . $colour['cmyk'] . ', ' . $colour['hex'] ?>
                                        </span>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="space"></div>

                        <div id="submit-btn" class="form-group col-xs-12">
                            <input type="submit" class="btn btn-primary btn-lg" value="<?= t('save'); ?>"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://www.helpdocsonline.com/v2/tooltips.js"></script>
