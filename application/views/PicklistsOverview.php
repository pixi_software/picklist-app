<div data-role="page" id="overview">
	<div id="navbar" class="navbar navbar-default">
		<script type="text/javascript">
			try{ace.settings.check('navbar' , 'fixed')}catch(e){}
		</script>
		<?php echo $releaseNote['script']; ?>
		<div class="navbar-container" id="navbar-container">
			<div class="pull-left">
				<a href="<?= site_url(''); ?>" class="navbar-brand">
					<img class="pixilogo pull-left" src="<?= base_url() ?>/assets/picklist/img/pixiHeader.svg" alt="pixi*" />
					<span class="moduleName"><?= t('app_title'); ?></span>
				</a>
			</div>
			<div class="pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="transparent">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<span><?= $username; ?></span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu-right dropdown-menu">
							<li><a href="<?=current_url()?>?pixi_lang=de" data-ajax="false"><span class="lang-xs" lang="de"></span> &nbsp;<?= t('german'); ?></a></li>
							<li><a href="<?=current_url()?>?pixi_lang=en" data-ajax="false"><span class="lang-xs" lang="en"></span> &nbsp;<?= t('english'); ?></a></li>
							<li class="divider"></li>
							<li><a href="/<?php echo session_name(); ?>/?logout" data-ajax="false"><?= t('logout'); ?></a></li>
						</ul>
					</li>
					<li class="transparent helpLink">
						<a href="<?= t('docu_help_link'); ?>" target="_blank" title="<?= t('docu_help_text'); ?>" tabindex="-1">
							<i class="fa fa-question"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="navbar toolbar">
		<div class="navbar-buttons pull-left">
			<ul class="nav">
				<li class="">
					<button href="<?= site_url('picklist'); ?>" class="toolbarButton" onClick="location.reload();"><i class="fa fa-refresh"></i></button>
				</li>
				<li class="">
					<button href="#" class="toolbarButton" type="button" data-toggle="collapse" data-target=".collapsePicklist"><i class="fa fa-check-circle-o"></i></button>
				</li>
				<li class="">
					<a href="<?= site_url('settings'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-gear"></i></a>
				</li>
				<li class="">
					<a href="<?= site_url('refill'); ?>" title="<?php echo t('refill'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-exchange"></i></a>
				</li>
			</ul>
		</div>
		<div class="navbar-buttons pull-right">
			<ul class="nav">
				<li class="">
					<a href="<?= site_url('stockout'); ?>" class="toolbarButton" data-axaj="false"><?= t('bin_shortfall'); ?></a>
				</li>
			</ul>
		</div>
	</div>
	<div data-role="content">
		<div class="margin20"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<?php echo $releaseNote['body']; ?>
					<?php 
						foreach($picklists as $picklist) {
							$createDate = new \DateTime($picklist['createDate']);
							$expiryDate = new \DateTime($picklist['PLHexpiryDate']);
						?>
						<?php if($picklist['Item_x0020_number'] > 0) { ?>
							<div class="panel panel-default" data-role="listview" data-inset="true">
								<div class="panel-heading picki" data-theme="<?= (isset($picklist['currentUser'])) ? 'a' : 'e'; ?>">
									<span class="collapseIcon"><i class="fa fa-arrow-down" data-toggle="collapse" data-target="#collapse<?= $picklist['PLHkey'] ?>"></i></span>
									<a href="<?= site_url('picklist/picklistDetails/'.$picklist['PLHkey'].'/'.$username); ?>" 
									data-transition="flip" 
									id="picklink_<?= $picklist['PLHkey'] ?>"
									data-ajax="false"
									<?php if(isset($picklist['currentUser']) && $picklist['currentUser'] != $username) { ?>
										onClick="javascript:$('#popup_start_detail_<?= $picklist['PLHkey'] ?>').popup('open');return false"
										<?php } ?>>
										<?= $picklist['PLHkey'] ?> <?php if(isset($picklist['currentUser'])) { echo '<i class="fa fa-warning"></i>'; }?>
										<span class="picklistLinkArrow"><i class="fa fa-arrow-right"></i></span>
									</a>
								</div>
								<div class="panel-body in" id="collapse<?= $picklist['PLHkey'] ?>">
									<div class="margin20"></div>
									<p><strong><?php if(isset($picklist['currentUser'])) { echo t('is_working_on_picklist', array($picklist['currentUser'])); }?></strong></p>
									<p><?= t('created_on'); ?>: <?= $createDate->format('d.m.Y H:i') ?> (<strong><?= t('expires'); ?>: <?= $expiryDate->format('d.m.Y H:i') ?></strong>)</p>
									<dl class="dl-horizontal picklistDefinitionList">
										<dt><?= t('created_by'); ?>:</dt>
										<dd><strong><?= $picklist['createEmp']; ?></strong></dd>
										<dt><?= t('start_end_bin'); ?>:</dt>
										<dd><strong><?= $picklist['FromBinName']; ?> | <?= $picklist['ToBinName']; ?></strong></dd>
										<dt><?= t('location'); ?>:</dt>
										<dd><strong><?= $locationId; ?></strong></dd>
										<dt><?= t('items'); ?>:</dt>
										<dd><strong><?= $picklist['Item_x0020_number']; ?></strong></dd>
										<dt><?= t('comment'); ?>:</dt>
										<dd><?= $picklist['PLcomment']; ?></dd>
									</dl>
									<div class="margin20"></div>
								</div>
							</div>
							<?php } else { ?>
							<div class="collapsePicklist in">
								<div class="panel panel-default">
									<div class="panel-heading">
										<span class="collapseIcon"><i class="fa fa-arrow-down" data-toggle="collapse" data-target="#collapse<?= $picklist['PLHkey'] ?>"></i></span>
										<h4><span class="picklistNumber"><?= $picklist['PLHkey'] ?> <?= t('picklist_completed'); ?></span><span class="picklistCheckArrow"><i class="fa fa-check"></i></span></h4>
									</div>
									<div class="panel-body collapse" id="collapse<?= $picklist['PLHkey'] ?>">
										<div class="margin20"></div>
										<dl class="dl-horizontal picklistDefinitionList">
											<dt><?= t('created_by'); ?>:</dt>
											<dd><strong><?= $picklist['createEmp']; ?></strong></dd>
											<dt><?= t('start_end_bin'); ?>:</dt>
											<dd><strong><?= $picklist['FromBinName']; ?> | <?= $picklist['FromBinName']; ?></strong></dd>
											<dt><?= t('location'); ?>:</dt>
											<dd><strong><?= $locationId; ?></strong></dd>
											<dt><?= t('items'); ?>:</dt>
											<dd><strong><?= $picklist['Item_x0020_number']; ?></strong></dd>
											<dt><?= t('comment'); ?>:</dt>
											<dd><?= $picklist['PLcomment']; ?></dd>
										</dl>
										<div class="margin20"></div>
									</div>
								</div>
							</div>
						<?php } // End Else ?>
						<?php if(isset($picklist['currentUser'])) { ?>
							<div data-role="popup" id="popup_start_detail_<?= $picklist['PLHkey'] ?>" data-dismissible="false">
								<p><?= t('picklist'); ?>: <?= $picklist['PLHkey'] ?></p>
								<h2><?= t('picklist_currently_picked'); ?></h2>
								<div class="margin40"></div>
								<a href="<?= site_url('picklist/picklistDetails/'.$picklist['PLHkey'].'/'.$username); ?>" class="btn btn-primary btn-block btn-lg"
								data-role="button" data-icon="alert" data-slide data-ajax="false"><?= t('takeover_picklist'); ?></a>
								<div class="margin20"></div>
								<a href="#" class="btn btn-default btn-block" onClick="$( '#popup_start_detail_<?= $picklist['PLHkey'] ?>' ).popup( 'close' ); return false;" data-rel="back" data-role="button" data-icon="delete" data-slide data-theme="e"><?= t('close'); ?></a>
								<div id="#notification_detail_<?= $picklist['PLHkey'] ?>"></div>
							</div>
						<?php } // End if isset currentUser ?>
					<?php } //End Foreach ?>
				</div>
			</div>
		</div>
	</div>	
</div>	
</div>
