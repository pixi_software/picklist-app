<?php

$count = 1;

?>

<div data-role="page">

<div id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="pull-left">
            <a href="<?= site_url(''); ?>" class="navbar-brand">
                <img class="pixilogo pull-left" src="<?= base_url() ?>/assets/picklist/img/pixiHeader.svg" alt="pixi*" />
                <span class="moduleName"><?= t('app_title'); ?></span>
            </a>
        </div>
        <div class="pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="transparent">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                    <span><?= $username; ?></span>
                    <i class="ace-icon fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu-right dropdown-menu">
                        <li><a href="/<?php echo session_name(); ?>/?logout" data-ajax="false"><?= t('logout'); ?></a></li>
                    </ul>
                </li>
                <li class="transparent helpLink">
                    <a href="<?= t('docu_help_link'); ?>" target="_blank" title="<?= t('docu_help_text'); ?>" tabindex="-1">
                        <i class="fa fa-question"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="navbar toolbar">
    <div class="navbar-buttons pull-left">
        <ul class="nav">
            <li class="">
                <a href="<?= site_url(); ?>" class="toolbarButton"><i class="fa fa-arrow-circle-o-left"></i></a>
            </li>
            <li class="">
                <a href="<?= site_url('stockout'); ?>" class="toolbarButton" onClick="location.reload();"><i class="fa fa-refresh"></i></a>
            </li>
            <li class="">
				<a href="<?= site_url('settings'); ?>" class="toolbarButton" data-ajax="false"><i class="fa fa-gear"></i></a>
			</li>
        </ul>
    </div>
</div>



<div data-role="content">
    <div class="margin40"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <form id="setSolved" action="<?= site_url('stockout/solveStockout'); ?>" method="POST">

                    <table class="table">
                        <tr>
                            <th></th>
                            <th><?= t('image'); ?></th>
                            <th><?= t('bin'); ?></th>
                            <th><?= t('item'); ?></th>
                            <th><?= t('quantity'); ?></th>
                            <th><?= t('in_bin_head'); ?></th>
                        </tr>

                            <?php foreach($stockouts as $stockout) { ?>

                                <tr>
                                    <td class="tableCheckbox">
                                        <input type="checkbox" value="<?= $stockout['id']; ?>" name="id[]" id="checkbox_<?php echo $count ?>" />
                                    </td>
                                    <td>
                                        <img src="<?php echo $stockout['image_link'] ?>" />
                                    </td>
                                    <td>
                                        <strong><?php echo $stockout['bin_name']; ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo $stockout['ean']; ?></strong> <br />
                                        <small><?php echo $stockout['item_number_supplier']; ?></small> <br />
                                        <small><?php echo $stockout['item_name']; ?></small>
                                    </td>
                                    <td class="text-right">
                                        <strong><?php echo $stockout['qty'] ?></strong>
                                    </td>
                                    <td class="text-right">
                                        <?= $stockout['total_items_on_bin'] ?>
                                    </td>
                                </tr>


                            <?php
                            $count++;
                            } // End Foreach ?>

                    </table>

                    <input type="submit" class="btn btn-primary btn-lg pull-right" value="<?= t('solve_shortfall'); ?>" />

                </form>

            </div>
        </div>
    </div>

</div>

<!--     <div data-role="panel" data-position="right" data-display="reveal" data-theme="b" id="nav-panel">
        <ul data-role="listview">
            <li><a href="<?= site_url('stockout'); ?>" data-ajax="true">Fehlbestand ansehen</a></li>
            <li><a href="/<?php echo session_name(); ?>/?logout" data-ajax="false">Logout</a></li>
        </ul>
    </div> -->

</div>
