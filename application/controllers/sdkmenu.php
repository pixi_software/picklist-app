<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

use Pixi\Ui\Menu\Menu;
use Pixi\Ui\Menu\MenuItem;
use Pixi\Ui\UIControllerInterface;

date_default_timezone_set('CET');

class SDKMenu extends PixiController implements UIControllerInterface
{

    function __construct()
    {
        parent::__construct();
    }

    function getAppTitle()
    {
        return 'Picklist App';
    }

    public function generateMenu()
    {
        $mainMenu = new Menu();
        return $mainMenu;
    }

    public function generateUserDropDown()
    {}

    public function generateLogDropDown()
    {}

    public function generateSearchForm()
    {}
}