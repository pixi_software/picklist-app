<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Stockout extends SDKMenu
{
    public $username;

    public function __construct()
    {
        parent::__construct();

        $this->username = \Pixi\AppsFactory\Environment::getUser();
    }

    public function index()
    {
        $rs = $this->customerdb->get('stockout')->result_array();

        $body = $this->load->view('StockoutOverview', array('stockouts' => $rs, 'username' => $this->username), true);

        $this->loadMainView(null, null, $body);
        return;
    }

    public function reportStockout()
    {
        $result = $this->customerdb->insert('stockout', array(
            'picklist_header_ref'   => $_POST['PLIheaderRef'],
            'ean'                   => $_POST['EanUpc'],
            'bin_name'              => $_POST['BinName'],
            'image_link'            => $_POST['PicLinkLarge'],
            'total_items_on_bin'    => $_POST['TotalSimilarItemsOnBin'],
            'qty'                   => $_POST['Qty'],
            'item_name'             => $_POST['ItemName'],
            'item_number_supplier'  => $_POST['ItemNrSuppl']
        ));

        if($result == true) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function solveStockout()
    {
        if(isset($_POST['id'])) {
            $this->customerdb->where_in('id', $_POST['id']);
            $this->customerdb->delete('stockout');
        }

        $this->index();
    }

}
