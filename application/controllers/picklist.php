<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

use Pixi\Ui\ReleaseNote\ReleaseNote;
use Pixi\Ui\ReleaseNote\ReleaseNoteElement;

class Picklist extends SDKMenu
{
    public $username;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('setup');
        $this->setup->setupTables();

        $this->username = \Pixi\AppsFactory\Environment::getUser();
    }

    public function index()
    {
        $this->customerdb->where('username', $this->username);
        $this->customerdb->where('setting_name', 'location');
        $locationSetting = $this->customerdb->get('user_settings')->result_array();

        if(empty($locationSetting)) {
            $locationId = '001';
        } else {
            $locationId = $locationSetting[0]['setting_value'];
        }

        $picklistHeaders = $this->pixiapi->pixiShippingGetPicklistHeaders(array('LocID' => $locationId))->getResultset();

        $picklists = array();
        foreach($picklistHeaders as $picklist) {

            $rs = $this->customerdb->get_where('picklist_status', array('picklist_header_ref' => $picklist['PLHkey']))->result_array();

            if(isset($rs[0]['username'])) {

                if (isset($picklist['Item_x0020_number']) && $picklist['Item_x0020_number'] == 0) {

                    $this->customerdb->delete('picklist_status', array('picklist_header_ref' => $picklist['PLHkey']));

                } else {

                    $picklist['currentUser'] = $rs[0]['username'];

                }

            }

            $picklists[] = $picklist;

        }

        $releaseNote = new ReleaseNote('');

        $releaseNote->addElement(
            new ReleaseNoteElement(
                t('whats_new'),
                '<div class="release-note-item clearfix">
                    <div class="image pull-left">
                        <a href="'. base_url() .'assets/app/screenshots/'.$this->config->config['language'].'/refill.jpg" data-rel="colorbox">
                            <img width="200" src="'. base_url() .'assets/app/screenshots/'.$this->config->config['language'].'/refill.jpg" />
                        </a>
                    </div>
                    <div class="description pull-left">
                        <p><strong>'.t('release_note_refill_title').'</strong></p>
                        <p>'.t('release_note_refill_text').'</p>
                    </div>
                </div>
                <div class="release-note-item clearfix">
                    <div class="image pull-left">
                        <a href="'. base_url() .'assets/app/screenshots/'.$this->config->config['language'].'/colour_codes.jpg" data-rel="colorbox">
                            <img width="200" src="'. base_url() .'assets/app/screenshots/'.$this->config->config['language'].'/colour_codes_thumb.jpg" />
                        </a>
                    </div>
                    <div class="description pull-left">
                        <p><strong>'.t('release_note_colorcodes_title').'</strong></p>
                        <p>'.t('release_note_colorcodes_text').'</p>
                    </div>
                </div>',
                'picklist-2.0',
                t('release_notes_docu_link'),
                t('more_info'),
                t('dont_show')
            )
        );

        if(count($picklistHeaders) == 0) {
            $body = $this->load->view('NoPicklists', array('username' => $this->username, 'releaseNote' => $releaseNote->generateHTML()), true);
        } else {
            $body = $this->load->view('PicklistsOverview', array('picklists' => $picklists, 'username' => $this->username, 'locationId' => $locationId, 'releaseNote' => $releaseNote->generateHTML()), true);
        }

        $this->loadMainView(null, null, $body);
        return;
    }

    public function picklistDetails($picklistHeaderKey)
    {
        $username = $this->username;

        $rs = $this->customerdb->get_where('picklist_status', array('picklist_header_ref' => $picklistHeaderKey))->result_array();

        if(isset($rs[0]['username']) && $rs[0]['username'] != $username) {
            $this->customerdb->where('picklist_header_ref', $picklistHeaderKey);
            $this->customerdb->update('picklist_status', array('username' => $username));
        } else if(!isset($rs[0]['username'])) {
            $this->customerdb->insert('picklist_status', array('picklist_header_ref' => $picklistHeaderKey, 'username' => $username));
        }

        $picklistDetails = $this->pixiapi->pixiShippingGetPicklistDetails(array('PicklistKey' => $picklistHeaderKey))->getResultset();

        $picklist = array();
        for($i = 0; $i <= count($picklistDetails)-1; $i++) {

            if(!isset($picklistDetails[$i]['PicLinkLarge'])) {
                $picklistDetails[$i]['PicLinkLarge'] = base_url().'/assets/picklist/img/no-image.png';
            }

            if(!isset($picklist[ $picklistDetails[$i]['EanUpc'].';'. $picklistDetails[$i]['BinName']])) {
                $picklist[ $picklistDetails[$i]['EanUpc'].';'. $picklistDetails[$i]['BinName']] = $picklistDetails[$i];

                if($i != count($picklistDetails)-1) {
                    $picklist[ $picklistDetails[$i]['EanUpc'].';'. $picklistDetails[$i]['BinName']]['nextBin'] = $picklistDetails[$i+1]['BinName'];
                }

            } else {
                $picklist[ $picklistDetails[$i]['EanUpc'].';'. $picklistDetails[$i]['BinName']]['Qty'] += $picklistDetails[$i]['Qty'];
                if(isset($picklistDetails[$i+1]['BinName'])) {
                    $picklist[ $picklistDetails[$i]['EanUpc'].';'. $picklistDetails[$i]['BinName']]['nextBin'] = $picklistDetails[$i+1]['BinName'];
                }
            }

        }

        $colours = $this->customerdb->get('colours')->result_array();
        $bin_colours = [];

        foreach ($colours as $colour) {
            $bin_colours[$colour['assigned_character']] = $colour['hex'];
        }

        $body = $this->load->view('PicklistDetails_Single', array(
            'picklistDetails' => $picklist,
            'total' => count($picklist),
            'picklistHeaderKey' => $picklistHeaderKey,
            'username' => $this->username,
            'bin_colours' => $bin_colours
        ), true);

        $this->loadMainView(null, null, $body);
        return;
    }

}
