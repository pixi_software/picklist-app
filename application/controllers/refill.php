<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Refill extends SDKMenu
{
    public $username;

    public function __construct()
    {
        parent::__construct();
        $this->username = \Pixi\AppsFactory\Environment::getUser();
    }

    public function index()
    {

        $picklist = array();
        $refillChecked = false;

        if (isset($_POST['FromLocID']) && isset($_POST['ToLocID'])) {
            $refillChecked = true;

            $picklistDetails = $this->pixiapi->pixiInventoryItemsToRefill(array('FromLocID' => $_POST['FromLocID'], 'ToLocID' => $_POST['ToLocID']))->getResultset();

            for ($i = 0; $i <= count($picklistDetails) - 1; $i++) {

                if (!isset($picklistDetails[$i]['PicLinkLarge'])) {
                    $picklistDetails[$i]['PicLinkLarge'] = base_url() . '/assets/picklist/img/no-image.png';
                }

                if (!isset($picklist[$picklistDetails[$i]['EanUpc'] . ';' . $picklistDetails[$i]['FromLocIDbinName']])) {
                    $picklist[$picklistDetails[$i]['EanUpc'] . ';' . $picklistDetails[$i]['FromLocIDbinName']] = $picklistDetails[$i];

                    if ($i != count($picklistDetails) - 1) {
                        $picklist[$picklistDetails[$i]['EanUpc'] . ';' . $picklistDetails[$i]['FromLocIDbinName']]['nextBin'] = $picklistDetails[$i + 1]['FromLocIDbinName'];
                    }

                } else {
                    $picklist[$picklistDetails[$i]['EanUpc'] . ';' . $picklistDetails[$i]['FromLocIDbinName']]['FromBinqty'] += $picklistDetails[$i]['FromBinqty'];
                    if (isset($picklistDetails[$i + 1]['FromLocIDbinName'])) {
                        $picklist[$picklistDetails[$i]['EanUpc'] . ';' . $picklistDetails[$i]['FromLocIDbinName']]['nextBin'] = $picklistDetails[$i + 1]['FromLocIDbinName'];
                    }
                }

            }
        }

        $locations = $this->pixiapi->pixiGetLocations()->getResultset();

        $body = $this->load->view(
            'RefillDetails_Single',
            array(
                'picklistDetails'   => $picklist,
                'total'             => count($picklist),
                'picklistHeaderKey' => 0,
                'username'          => $this->username,
                'locations'         => $locations,
                'refillChecked'     => $refillChecked
            ),
            true
        );

        $this->loadMainView(null, null, $body);
        return;
    }

}
