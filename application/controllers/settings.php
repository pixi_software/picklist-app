<?php

/**
 * Copyright (C) 2015 pixi* Software GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Settings extends SDKMenu
{
    public $username;

    public function __construct()
    {
        parent::__construct();

        $this->username = \Pixi\AppsFactory\Environment::getUser();
    }

    public function index()
    {                
        if(isset($_POST['location'])) {
            
            $this->customerdb->query('
                INSERT INTO 
                    user_settings (
                        `username`, 
                        `setting_name`, 
                        `setting_value`
                    )
                VALUES (?,?,?)
                ON DUPLICATE KEY UPDATE
                    setting_value = ?
            ', array(
                $this->username,
                'location',
                $_POST['location'],
                $_POST['location']
            ));

            foreach ($_POST['colours'] as $key => $val) {
                $this->customerdb->update('colours', array('assigned_character' => $val), "id = $key");
            }
            
        }
        
        $this->customerdb->where('username', $this->username);
        $this->customerdb->where('setting_name', 'location');
        $locationSetting = $this->customerdb->get('user_settings')->result_array();
        
        if(empty($locationSetting)) {
            $locationId = '001';
        } else {
            $locationId = $locationSetting[0]['setting_value'];
        }
        
        $locationsResult = $this->pixiapi->pixiGetLocations()->getResultset();
        
        $locations = array();
        foreach($locationsResult as $location) {
            
            $location['selected'] = '';
            
            if($location['LocID'] == $locationId) {
                $location['selected'] = 'selected';
            }
            
            $locations[] = $location;
            
        }

        $colours = $this->customerdb->get('colours')->result_array();
        
        $body = $this->load->view('Settings', array('username' => $this->username, 'locations' => $locations, 'colours' => $colours), true);

        $this->loadMainView(null, null, $body);
    }

}
